// 1st Activity 

function Person(firstName, lastName, age, city, province) {
    this.name = firstName + ' ' + lastName;
    this.age = age;
    this.address = city + ', ' + province;
    this.introduceSelf = function() {
        return console.log("Hello! I am " + this.name + ", " + this.age + " years old, and currently living in " + this.address + ".");
    }
}

let juan = new Person("Juan", "Dela Cruz", 21, "Quezon City", "Metro Manila");
juan.introduceSelf()


// =====================================================================
// 2nd Activity (if else)

let age = parseInt(prompt("Input Age: "));
let voter;

function qualifiedVoter(ageValidate)  {
    if (ageValidate >= 18) {
        voter = "You're qualified to vote!";
    } else {
        voter = "Sorry, You're too young to vote.";
    }
    return voter;
}

alert(qualifiedVoter(age));
console.log(qualifiedVoter(age));


// =====================================================================
// 3rd Activity (switch case)

let inputMonth = prompt("Enter month number: ");
switch (parseInt(inputMonth)) {
    case 1: {
        alert("Total Number of days for January: 31");
        console.log("Total Number of days for January: 31");
        break;
    }
    case 2: {
        alert("Total Number of days for February: 28");
        console.log("Total Number of days for February: 28");
        break;
    }
    case 3: {
        alert("Total Number of days for March: 31");
        console.log("Total Number of days for March: 31");
        break;
    }
    case 4: {
        alert("Total Number of days for April: 30");
        console.log("Total Number of days for April: 30");
        break;
    }
    case 5: {
        alert("Total Number of days for May: 31");
        console.log("Total Number of days for May: 31");
        break;
    }
    case 6: {
        alert("Total Number of days for June: 30");
        console.log("Total Number of days for June: 30");
        break;
    }
    case 7: {
        alert("Total Number of days for July: 31");
        console.log("Total Number of days for July: 31");
        break;
    }
    case 8: {
        alert("Total Number of days for August: 31");
        console.log("Total Number of days for August: 31");
        break;
    }
    case 9: {
        alert("Total Number of days for September: 30");
        console.log("Total Number of days for September: 30");
        break;
    }
    case 10: {
        alert("Total Number of days for October: 31");
        console.log("Total Number of days for October: 31");
        break;
    }
    case 11: {
        alert("Total Number of days for November: 30");
        console.log("Total Number of days for November: 30");
        break;
    }
    case 12: {
        alert("Total Number of days for December: 31");
        console.log("Total Number of days for December: 31");
        break;
    }
    default: {
        alert("Invalid input! Please enter the month between 1-12");
        console.log("Invalid input! Please enter the month between 1-12");
        break;
    }
}


// =====================================================================
// 4th Activity (leap year)
let inputYear = parseInt(prompt("Enter year: "));
let outputYear;

function checkYear(year) {
    if ( (year % 400 === 0) || (year % 4 === 0 && year % 100 !== 0) ) {
        outputYear = year + " is a leap year";
        alert(outputYear);
    } else {
        outputYear = "Not a leap year!";
        alert(outputYear);
    }
    return outputYear;
}

console.log(checkYear(inputYear));

// =====================================================================
// 5th Activity (Loops)

let userInput = parseInt(prompt("Enter a Number: "));

function countdown (number) {
    for (let i = number; i >= 0; i--) {
        console.log(number);
        number -= 1;
    }
}

countdown(userInput);

